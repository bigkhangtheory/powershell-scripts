#requires -version 3
<#
    .SYNOPSIS
        Retrieve information on Active Directory and run a series of health checks on related services
    .DESCRIPTION
        This script retrieves Active Directory information and runs health checks on the following:
            - Network Connectivity (Ping)
            - Netlogon
            - NTDS
            - DCdiag
            - Replication Health
            - SYSVOL Health
    .INPUTS
        System.Management.Automation.PSCredential
    .OUTPUTS
        System.Management.Automation.PSCustomObject
    .NOTES
        Revision:   1
        Author:     Khang M. Nguyen <knguyen@mapcommunications.com>
        Created:    2020-11-05
        Updated:
        Changes:
#>
[CmdletBinding()]
param(
    [Parameter(Mandatory = $false)]
    [Alias('RunAs')]
    [System.Management.Automation.Credential()]
    [PSCredential]
    $Credential = [System.Management.Automation.PSCredential]::Empty
)
function Get-DefaultMessage {
    <#
        .SYNOPSIS
            Helper function to show default message used in VERBOSE/DEBUG/WARNING
            and prepend with a timestamp
        .PARAMETER Message
            Message to be displayed
        .INPUTS
            System.String
        .OUTPUTS

        .EXAMPLE
            Get-DefaultMessage -Message "Issue connecting to host"
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [System.String]$Message
    )
    begin {
        # Construct time for output
        $time = Get-Date -Format 'HH:mm:ss.fff'

        # Construct date for output
        $date = Get-Date -Format 'MM-dd-yyyy'

        # Construct context
        #$context = $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)

        # Construct command
        $command = (Get-Variable -Scope 1 -Name MyInvocation -ValueOnly).MyCommand.Name
    }
    process {
        $output = [String]::Format('{0,-12} {1,-10} {2,-28} {3,-30}', '[' + $time, $date + ']', '[' + $command + ']', '- ' + $Message)
        Write-Output $output
    }
    end {}
}#Get-DefaultMessage


function Get-DefaultSplatting {
    <#
        .SYNOPSIS
            Return hashtable holding default key/value pairs
        .INPUTS
            System.String
        .OUTPUTS
            System.Collections.Hashtable
        .EXAMPLE
            Get-DefaultSplatting
    #>
    [CmdletBinding()]
    param()
    return [System.Collections.Hashtable] @{
        Verbose           = $true
        InformationAction = 'Continue'
        WarningAction     = 'Continue'
        ErrorAction       = 'Stop'
        ErrorVariable     = 'ProcessError'
    }
}#Get-DefaultSplatting


function Get-ADForestInfo {
    <#
        .SYNOPSIS
            Retrieve general information on Active Directory Forest
        .INPUTS
            System.Management.Automation.PSCredential
        .OUTPUTS
            System.Management.Automation.PSCustomObject
    #>
    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential = [System.Management.Automation.PSCredential]::Empty
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')
        $Splatting = Get-DefaultSplatting
        if ($PSBoundParameters['Credential']) {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Credentials specified.')
            $Splatting.Credential = $Credential
        }
    }#begin
    process {
        Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest .....')
        try {
            $myForest = Get-ADForest @Splatting
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest ..... FINISHED.')
            [PSCustomObject][Ordered] @{
                ForestName         = $myForest.Name
                ForestMode         = $myForest.ForestMode
                NumberOfDomains    = ($myForest.Domains).Count
                Domains            = $myForest.Domains
                NumberOfSites      = ($myForest.Sites).Count
                ForestSiteNames    = $myForest.Sites
                GlobalCatalogs     = $myForest.GlobalCatalogs
                DomainNamingMaster = $myForest.DomainNamingMaster
                SchemaMaster       = $myForest.SchemaMaster
            }
        }
    }#process
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADForestInfo


function Get-ADSiteInfo {
    <#
        .SYNOPSIS
            Retrieve Active Directory site and subnet information
        .INPUTS
            System.String
        .OUTPUTS
            System.Management.Automation.PSCustomObject
        .EXAMPLE
            Get-ADSiteInfo -Forest "mapcom.local"
    #>
    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [System.String]$Forest
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Querying Active Directory Forest Directory Context .....')
        try {
            $forestContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', $Forest)
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Querying Active Directory Forest Directory Context ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Querying Active Directory Forest Directory Context ..... FINISHED.')
        }
    }#begin
    process {
        Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest Sites .....')
        try {
            [Array]$adSites = [System.DirectoryServices.ActiveDirectory.Forest]::GetForest($forestContext).Sites
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest Sites ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Forest Sites ..... FINISHED.')
            foreach ($mySite in $adSites) {
                [PSCustomObject][Ordered] @{
                    SiteName          = $mySite.Name
                    Domains           = $mySite.Domains
                    SiteServers       = $mySite.Servers
                    Subnets           = $mySite.Subnets
                    BridgeheadServers = $mySite.BridgeheadServers
                }
            }
        }
    }
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADSiteInfo


function Get-ADDomainInfo {
    <#
        .SYNOPSIS
            Retrieve general information on Active Directory Forest
        .INPUTS
            System.Management.Automation.PSCredential
            System.String
        .OUTPUTS
            System.Management.Automation.PSCustomObject
        .EXAMPLE
            Get-DomainInfo -Domain "mapcom.local" -Credential (Get-Credential)
    #>
    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential = [System.Management.Automation.PSCredential]::Empty,

        [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
        [System.String]$Domain
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')
        $Splatting = Get-DefaultSplatting

        if ($PSBoundParameters['Credential']) {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Credentials specified.')
            $Splatting.Credential = $Credential
        }
    }#begin
    process {
        if ($PSBoundParameters['Domain']) {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] Adding $Domain to hashtable")
            $Splatting.Identity = $Domain
        }

        Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Domain .....')
        try {
            $myDomain = Get-ADDomain @Splatting
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Domain ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Domain ..... FINISHED.')
            [PSCustomObject][Ordered] @{
                DomainName        = $myDomain.NetBiosName
                DomainMode        = $myDomain.DomainMode
                DNSRoot           = $myDomain.DNSRoot
                DistinguishedName = $myDomain.DistinguishedName
                Forest            = $myDomain.Forest
                DomainControllers = $myDomain.ReplicaDirectoryServers
                PDCEmulator       = $myDomain.PDCEmulator
                RIDMaster         = $myDomain.RIDMaster
            }
        }
    }
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADDomainInfo


function Get-ADSchemaInfo {
    <#
        .SYNOPSIS
            Retrieve information on the Active Directory schema
        .INPUTS
            System.Management.Automation.PSCredential
        .OUTPUTS
            System.Management.Automation.PSCustomObject
        .EXAMPLE
            Get-ADSchemaInfo -Credential (Get-Credential)
    #>
    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential = [System.Management.Automation.PSCredential]::Empty
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')
        $Splatting = Get-DefaultSplatting

        if ($PSBoundParameters['Credential']) {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Credentials specified.')
            $Splatting.Credential = $Credential
        }

        try {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Retrieving Root of Directory Information Tree .....')
            $Identity = (Get-ADRootDSE @Splatting).schemaNamingContext
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Retrieving Root of Directory Information Tree ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Retrieving Root of Directory Information Tree ...... SUCCEEDED.')
            $Splatting.Identity = $Identity
            $Splatting.Property = '*'
        }
    }#begin
    process {
        Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Schema .....')
        try {
            $mySchema = (Get-ADObject @Splatting)
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Schema ..... FAILED.')
            Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESS] Querying Active Directory Schema ..... FINISHED.')
            [PSCustomObject][Ordered] @{
                CanonicalName     = $mySchema.CanonicalName
                DistinguishedName = $mySchema.DistinguishedName
                Created           = $mySchema.Created
                SchemaValue       = $mySchema.objectVersion
                SchemaName        = switch ($mySchema.objectVersion) {
                    88 { 'Windows Server 2019' }
                    87 { 'Windows Server 2016' }
                    69 { 'Windows Server 2012 R2' }
                    56 { 'Windows Server 2012' }
                    47 { 'Windows Server 2008 R2' }
                    44 { 'Windows Server 2008' }
                    31 { 'Windows Server 2003 R2' }
                    30 { 'Windows Server 2003' }
                }
            }
        }#finally
    }#process
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADSchemaInfo


function Get-ADDomainControllerInfo {
    <#
        .SYNOPSIS
            Retrieve information on an Active Directory Domain Controller server
        .INPUTS
            System.Management.Automation.PSCredential
            System.String
        .OUTPUTS
            System.Management.Automation.PSCustomObject
        .EXAMPLE
            Get-ADDomainControllerInfo -Server "dc-ches-00" -Credential (Get-Credential)
    #>
    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]
    param (
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential = [System.Management.Automation.PSCredential]::Empty,

        [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
        [ValidateScript( { Test-Connection -ComputerName $_ -Count 1 })]
        [System.String]$Server
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting...')
        $Splatting = Get-DefaultSplatting

        if ($PSBoundParameters['Credential']) {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Credentials specified.')
            $Splatting.Credential = $Credential
        }
    }#begin
    process {
        if ($PSBoundParameters['Server']) {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] Adding $Server to hashtable.")
            $Splatting.Server = $Server
        }
        Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] Querying Active Directory Domain Controller `"$Server`" .....")
        try {
            $myDC = Get-ADDomainController @Splatting
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] Querying Active Directory Domain Controller `"$Server`" ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] Querying Active Directory Domain Controller `"$Server`" ..... FINISHED.")
            [PSCustomObject][Ordered] @{
                Name            = $myDC.Name
                isEnabled       = $myDC.Enabled
                isReadOnly      = $myDC.isReadOnly
                IsGlobalCatalog = $myDC.IsGlobalCatalog
                Forest          = $myDC.Forest
                Domain          = $myDC.Domain
                OSVersion       = $myDC.OperatingSystemVersion
                OSDisplayName   = $myDC.OperatingSystem
                HostName        = $myDC.HostName
                IPv4Address     = $myDC.IPv4Address
                LdapPort        = $myDC.LdapPort
                SslPort         = $myDC.SslPort
                Site            = $myDC.Site

            }
        }#finally
    }#process
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADDomainControllerInfo


function Get-ADServiceStatus {
    <#
        .SYNOPSIS
            Retrieve health status of specified service on specified machine
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential,

        [Parameter(Mandatory = $true)]
        [ValidateScript( { Test-Connection -ComputerName $_ -Count 1 })]
        [System.String]$ComputerName

    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")

        # Checking the following services for running status on Domain Controllers
        $serviceList = [System.Collections.ArrayList]@(
            'EventSystem',
            'RpcSs',
            'NTDS',
            'DnsCache',
            'NtFrs',
            'IsmServ',
            'kdc',
            'SamSs',
            'LanmanServer',
            'LanmanWorkstation',
            'w32time'
            'NETLOGON',
            'DNS'
        )

        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')

        # Storing remote execution parameters in hashtable
        $Splatting = @{
            Credential   = $Credential
            ComputerName = $ComputerName
            ScriptBlock  = { param($service) Get-Service -Name $service }
            AsJob        = $true
            ErrorAction  = 'SilentlyContinue'
        }
        $timeout = '60'

        # Storing results of service health checks in array
        $results = [System.Collections.ArrayList]@()

    }#begin

    process {
        # For each service, remotely execute a query to obtain the current running status
        foreach ($service in $serviceList) {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName checking status of service `"$service`" .....")
            try {
                # Adding service to the hashtable containing our remote exection parameters
                $Splatting.ArgumentList = $service
                $serviceCheck = Invoke-Command @Splatting
                # Allow remote job to run for 60 seconds
                Wait-Job -Job $serviceCheck -Timeout $timeout | Out-Null
            } catch {
                Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName checking status of service `"$service`" ..... FAILED.")
                Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
                Continue
            } finally {
                # Remote execution does not finish within the timeout value
                if ($serviceCheck.State -like 'Running') {
                    Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName checking status of service `"$service`" ..... TIMED OUT.")
                    Stop-Job -Job $serviceCheck
                    $results += [System.Collections.HashTable] @{
                        Object          = [String]::Format('{0, -20} {1, -30} {2,-20}', $ComputerName, $service, 'Timeout')
                        ForegroundColor = 'Yellow'
                    }
                }
                # Remote exection finishes within timeout value
                else {
                    Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName checking status of service `"$service`" ..... FINISHED.")
                    $job = Receive-Job -Job $serviceCheck
                    $results += [System.Collections.HashTable] @{
                        Object          = [String]::Format('{0,-20} {1,-30} {2,-20}', '[host] ' + $ComputerName, '[service] ' + $job.Name, '[' + $job.Status + ']')
                        ForegroundColor = switch ($job.Status) {
                            'Running' { 'DarkGreen' }
                            'Failed' { 'DarkRed' }
                        }
                    }
                }
            }#finally
        }#foreach
        $results
    }#process
    end { Write-Verbose -Message (Get-DefaultMessage -Message '[END] Execution.') }
}#Get-ADServiceStatus


function Get-ADDCDiagStatus {
    <#
        .SYNOPSIS
            Retrieve health status of DCDiag tests
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential,

        [Parameter(Mandatory = $true)]
        [ValidateScript( { Test-Connection -ComputerName $_ -Count 1 })]
        [System.String]$ComputerName
    )
    begin {
        Write-Verbose -Message (Get-DefaultMessage -Message "[BEGIN] Execution.")
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')
        $Splatting = @{
            Credential   = $Credential
            ComputerName = $ComputerName
            ScriptBlock  = { param($test) dcdiag.exe /test:$test /v }
            AsJob        = $true
            ErrorAction  = 'SilentlyContinue'
        }

        $testList = [System.Collections.ArrayList]@(
            'Replications', 'NCSecDesc', 'NetLogons', 'Advertising',
            'KnowsOfRoleHolders', 'Intersite', 'FSMOCheck', 'RidManager',
            'MachineAccount', 'Services', 'OutboundSecureChannels',
            'ObjectsReplicated', 'frssysvol', 'frsevent', 'kccevent',
            'systemlog', 'CheckSDRefDom', 'VerifyReplicas', 'CrossRefValidation',
            'VerifyReferences', 'Topology', 'DNS /DnsBasic'
        )

        $timeout = '60'

        $results = [System.Collections.ArrayList]@()

        # Regex for English System
        [Regex]$regexDCDiag = '\s*\.+\s(?<Target>\w+\W*\.*\w+\W*\w+)\s(?<Result>\w+)\stest\s\r*\n*\s*(?<Test>\w+)'
        <#
                                \s* : match any white-space char (zero or more times)
                                \.+  : match literal period char (one or more times)
                                \s   : match any white space char
                                (?<Target>\w+\.*\w+)    : match any word char (one or more times)
                                                          match literal period (zero or more times)
                                                          match any word char (one or more times)
                              #>
    }#begin
    process {
        foreach ($test in $testList) {
            Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName running DCDiag Test `"$test`" .....")
            try {
                $Splatting.ArgumentList = $test
                $testCheck = Invoke-Command @Splatting
                Wait-Job -Job $testCheck -Timeout $timeout | Out-Null
            } catch {
                Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName running DCDiag Test `"$test`" ..... FAILED.")
                Write-Error -Message (Get-DefaultMessage -Message "[PROCESS] $($Error[0].Exception.Message)")
                Continue
            } finally {
                if ($testCheck.State -like 'Running') {
                    Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName running DCDiag Test `"$test`" ..... TIMED OUT.")
                    Stop-Job $testCheck
                    $results += [System.Collections.HashTable] @{
                        Object          = [String]::Format("{0} `t {1} DCDiag timeout", $ComputerName)
                        ForegroundColor = 'Yellow'
                    }
                } else {
                    Write-Verbose -Message (Get-DefaultMessage -Message "[PROCESS] $ComputerName running DCDiag Test `"$test`" ..... FINISHED.")
                    $regexMatches = $regexDCDiag.Matches($(Receive-Job -Job $testCheck))
                    foreach ($match in $regexMatches) {
                        $result = '' | Select-Object Target, Test, Result
                        $result.Target = $match.Groups.Item('Target').Value
                        $result.Test = $match.Groups.Item('Test').Value
                        $result.Result = $match.Groups.Item('Result').Value
                        if ($result.Test -ne 'Connectivity') {
                            $results += [System.Collections.Hashtable] @{
                                Object          = [String]::Format('{0, -20} {1, -25} {2,-30} {3,-20}', '[host] ' + $ComputerName, '[target] ' + $result.Target, '[test] ' + $result.Test, '[' + $result.Result + ']')
                                ForegroundColor = switch ($result.Result) {
                                    'passed' { 'DarkGreen' }
                                    'failed' { 'DarkRed' }
                                }
                            }
                        }#if
                    }#foreach
                }#else
            }#finally
        }#foreach
        $results
    }#process
    end { Write-Verbose -Message (Get-DefaultMessage -Message "[END] Execution.") }
}#Get-ADDCDiagStatus


function Invoke-ADHealthCheck {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false)]
        [Alias('RunAs')]
        [System.Management.Automation.Credential()]
        [PSCredential]
        $Credential = [System.Management.Automation.PSCredential]::Empty
    )
    begin {
        try {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module ActiveDirectory.')
            if (-not (Get-Module -Name ActiveDirectory)) {
                Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module ActiveDirectory...')
                Import-Module -Name ActiveDirectory -ErrorAction Stop
            }
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Error while importing the module ActiveDirectory')
            Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Module ActiveDirectory successfully imported.')
        }

        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Create hashtable splatting.')

        [Hashtable]$Splatting = Get-DefaultSplatting
        if ($PSBoundParameters['Credential']) {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Credentials specified.')
            $Splatting.Credential = $Credential
        }

        $myForest = Get-ADForestInfo @Splatting
        $mySchema = Get-ADSchemaInfo @Splatting
        $mySites = Get-ADSiteInfo -Forest $myForest.ForestName -Verbose
        $myDomains = [System.Collections.ArrayList]@()
        foreach ($domain in $myForest.Domains) {
            $myDomains += Get-ADDomainInfo @Splatting -Domain $domain
        }
        $myDCs = [System.Collections.ArrayList]@()
        foreach ($dc in $myForest.GlobalCatalogs) {
            $myDCs += Get-ADDomainControllerInfo @Splatting -Server $dc
        }
        $myADServices = [System.Collections.ArrayList]@()
        foreach ($dc in $myDCs.Name) {
            $myADServices += Get-ADServiceStatus @Splatting -ComputerName $dc
        }
        $myDCDiag = [System.Collections.ArrayList]@()
        foreach ($dc in $myDCs.Name) {
            $myDCDiag += Get-ADDCDiagStatus @Splatting -ComputerName $dc
        }
    }#begin
    process {
        Write-Host ('# Active Directory Forest Information' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        $myForest
        Write-Host ('# Active Directory Schema Information' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        $mySchema
        Write-Host ('# Active Directory Domain Information' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        $myDomains
        Write-Host ('# Active Directory Sites Information' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        $mySites
        Write-Host ('# Active Directory Domain Controller Information' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        $myDCs
        Write-Host ('# Active Directory Services Health Status' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        foreach ($dc in $myADServices) {
            foreach ($result in $dc) { Write-Host @result }
        }
        Write-Host ('# Active Directory DCDiag Tests' | ConvertFrom-Markdown -AsVT100EncodedString).VT100EncodedString -NoNewline
        foreach ($dc in $myDCDiag) {
            foreach ($result in $dc) { Write-Host @result }
        }
    }#process
    end {}#end
}
Invoke-ADHealthCheck -Credential $Credential
