﻿<#
    Clear-WindowsUpdateCache
    Created by Xavier Acosta

    .SYNOPSIS
    Clears cache to free up drive space
    Needs to be run with admin privelages

    .DESCRIPTION
    Clears the update cache to free up drive space

    .EXAMPLE
    Clear-WindowsUpdateCache

#>

#Try to stop update service so that cache can be cleared
try {
    Stop-Service -Name "wuauserv" -ErrorAction Stop

} catch {
    "Could not stop Windows Update Service (wuauserv)"
}

#Try to remove all items in cache folder
try {
    Remove-Item C:\$env:WINDIR\SoftwareDistribution\Download\* -ErrorAction Stop

} catch {
    "Could not remove cache files at C:\Windows\SoftwareDistribution\Download"
}    

#Try to start the update service
try {
    Start-Service -Name "wuauserv" -ErrorAction Stop

} catch {
    "Could not start the Windows Update Service (wuauserv)" 
}

echo "Successfully cleared the Windows Update Cache"