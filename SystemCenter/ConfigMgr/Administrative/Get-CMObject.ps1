﻿<#  Created by Xavier Acosta
    .SYNOPSIS
    Gets the objects of ObjectType in the CurrentDirectory variable and returns an interactable object

    .DESCRIPTION
    Finds objects of CMObjectType in CurrentDirectory.
    Then it will convert the objects to CMI-ResultObjects (Microsoft.ConfigurationManagement.ManagementProvider.IResultObject)
    and finally return the objects.
    If directory does not have any of CMObjectType then nothing is returned.

    .PARAMETER
    CurrentDirectory
    Specifies the CurrentDirectory to retrieve the objects from
    
    .PARAMETER
    CMObjectType
    Specifies the type of object to query for objects are:
    
      -TYPE-                     -Variable-
    Application	               ApplicationLatest
    Package	                   Package	
    Query	                   Query	
    Software Metering Rule	   MeteredProductRule	
    Configuration Item	       ConfigurationItemLatest	
    Configuration Baseline	   ConfigurationBaselineInfo	
    Operating System Image	   OperatingSystemInstallPackage	
    Operating System Package   ImagePackage	
    User State Migration	   StateMigration	
    Boot Image	               BootImagePackage	
    Task Sequence	           TaskSequencePackage	
    Driver Package	           DriverPackage	
    Driver	                   Driver	
    Software Update	           SoftwareUpdate	
    Collection	               Collection	

    .PARAMETER
    ComputerName
    Specifies the Computer Name of the Management Point Server, defaults to sccm-mgmt.cs.odu.edu

    .PARAMETER
    SiteCode
    Specifies the Site Code, defaults to CS3

    .EXAMPLE
    Get-CMObject -CurrentDirectory "CS3:\SoftwareUpdates\Operating Systems\Windows 10\1511\Targeted Updates -CMObjectType "SoftwareUpdate" -SiteCode "CS3" -ComputerName "sccm-mgmt.cs.odu.edu"
#>

#Parameter of CurrentDirectory required
Param (

        [Parameter(Mandatory=$true)]
        [string] $CurrentDirectory,
        [Parameter(Mandatory=$true)]
        [string] $CMObjectType,
        $SiteCode = "CS3",
        $ComputerName = "sccm-mgmt.cs.odu.edu"
)

#Attempt to load Configuration Manager module 
cd "C:\Program Files (x86)\Microsoft Configuration Manager\AdminConsole\bin"
Import-Module .\ConfigurationManager.psd1

#Find the ContainerNodeID value of CurrentDirectory and save it to variable
try {
    $ContainerNodeID = (Select-Object -Property ContainerNodeID -InputObject(Get-Item $CurrentDirectory)).ContainerNodeID
    
} catch {
    "Error retrieving the CurrentDirectory ContainerNodeID"
}

#Find the ObjectType variable for the folder
try{
    $ObjectType = (Select-Object -Property ObjectType -InputObject( Get-Item $CurrentDirectory)).ObjectType

} catch{
    "Error retrieving the CurrentDirectory ObjectType"
}

#Store Query based on ObjectType
$Query = "select * from SMS_$CMObjectType where ModelName is in
        (select InstanceKey from SMS_ObjectContainerItem where ObjectType='$ObjectType' and ContainerNodeID='$ContainerNodeID')"

#Find the objects inside of the CurrentDirectory and store them in a variable
try{   
    $ObjectsInCurrentDirectory = Get-WmiObject -ComputerName $ComputerName -Namespace "ROOT\SMS\Site_$SiteCode" -Query $Query

} catch{    
    "Error retrieving WMIObjects at $ComputerName in Namespace ROOT\SMS\Site_$SiteCode with Query:
     $Query"
}

#ConvertedObjects array to contain the objects after conversion
$ConvertedObjects= [System.Collections.ArrayList]@()

#Move to the site directory to interact with objects
cd $SiteCode':\'

#Convert the objects into CMIResultObjects and append them to ConvertedObjects
ForEach($Object in $ObjectsInCurrentDirectory) {

    try{
        $ConvertedObject = ConvertTo-CMIResultObject -InputObject $Object 
    
    } catch{
        
        "Error Converting Object to IResultObject"
    }
    
    [void] $ConvertedObjects.Add($ConvertedObject.Properties)
  
}

return $ConvertedObjects