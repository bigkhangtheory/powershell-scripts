<#
  .SYNOPSIS
    Remove One Drive package from Windows 10 OS
  .DESCRIPTION
    Removes One Drive package, application, and *.lnk files
  .EXAMPLE
    .\Remove-OneDrive.ps1
  .NOTES
    Khang M. Nguyen
    bigkhangtheory@gmail.com
    @bigkhangtheory (ig)
  .NOTES
    Do not run this script unless you understand it. Blindly running
    scripts as an administrator on a machine is a irresponsible way to
    get malware, delete or corrupt files, or just ruin a machine. Read
    through this script and ensure that it suits your desired outcome
#>

$SETUP32 = "C:\\Windows\\System32\\OneDriveSetup.exe"
$SETUP64 = "C:\\Windows\\SysWOW64\\OneDriveSetup.exe"

taskkill.exe /F /IM "OneDrive.exe"
taskkill.exe /F /IM "Explorer.exe"

if (Test-Path $SETUP32) {
  Start-Process $SETUP32 -ArgumentList "/uninstall" -Wait
}
if (Test-Path $SETUP64) {
  Start-Process $SETUP64 -ArgumentList "/uninstall" -Wait
}

Remove-Item -Path "C:\\Windows\\ServiceProfiles\\LocalService\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\OneDrive.lnk" -Force
Remove-Item -Path "C:\\Windows\\ServiceProfiles\\NetworkService\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\OneDrive.lnk" -Force
# Remove the automatic start item for OneDrive from the default user profile registry hive
Start-Process C:\\Windows\\System32\\Reg.exe -ArgumentList "Load HKLM\\Temp C:\\Users\\Default\\NTUSER.DAT" -Wait
Start-Process C:\\Windows\\System32\\Reg.exe -ArgumentList "Delete HKLM\\Temp\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /v OneDriveSetup /f" -Wait
Start-Process C:\\Windows\\System32\\Reg.exe -ArgumentList "Unload HKLM\\Temp" -Wait
Start-Process -FilePath C:\\Windows\\Explorer.exe -Wait