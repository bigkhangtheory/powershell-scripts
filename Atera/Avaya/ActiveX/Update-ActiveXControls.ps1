<#
    .SYNOPSIS
        Check for up-to-date version of CCMA ActiveX Controls, otherwise upgrade
    .NOTES
        Author:     Khang M. Nguyen
        Created:    2020-11-22
#>
[CmdletBinding()]
param ()

begin {
    # Helper function
    function Get-DefaultMessage {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $Message
        )
        # Construct time for output
        $time = Get-Date -Format 'HH:mm:ss.fff'
        # Construct date for output
        $date = Get-Date -Format 'MM-dd-yyyy'
        # get hostname
        $hostName = "$env:ComputerName"
        $output = [String]::Format('{0} {1} {2} {3}', '[' + $time, $date + ']', '[' + $hostName + ']', '- ' + $Message)
        Write-Output $output
    }#Get-DefaultMessage


    function Invoke-FileDownload {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $Uri,

            [Parameter(Mandatory = $true)]
            [System.String]
            $OutFile
        )
        Write-Verbose -Message (Get-DefaultMessage -Message "Downloading file $Outfile ...")
        try {
            Invoke-WebRequest -Uri $Uri -OutFile $OutFile
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message "Downloading file $OutFile ... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message "Downloading file $OutFile ... COMPLETED.")
        }
        return $OutFile
    }#Invoke-FileDownload


    function Invoke-AppInstall {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $InstallFile
        )
        begin {
            $dateStamp = Get-Date -Format yyyyMMddTHHmmss
            $logPath = "$env:TEMP"
            $logFile = [String]::Format("{0}_{1}.log", "msiexec", "$dateStamp")

            $msiArguments = @(
                "/i"
                ('"{0}"' -f $InstallFile)
                "/qn"
                "/norestart"
                "/L*v"
                "$logPath\$logFile"
            )

            $Splatting = @{
                FilePath     = "msiexec.exe"
                ArgumentList = $msiArguments
                Wait         = $true
                NoNewWindow  = $true
                Verbose      = $true
                ErrorAction  = 'Stop'
            }
        }
        process {
            Write-Verbose -Message (Get-DefaultMessage -Message "Installing $InstallFile ...")
            try {
                Start-Process @Splatting
            } catch {
                Write-Verbose -Message (Get-DefaultMessage -Message "Installing $InstallFile .... FAILED.")
                Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
            } finally {
                Write-Verbose -Message (Get-DefaultMessage -Message "Installing $InstallFile .... COMPLETED.")
            }
        }
        end {}
    }#Invoke-AppInstall


    function Invoke-AppUninstall {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $IdentifyingNumber
        )
        begin {
            $dateStamp = Get-Date -Format yyyyMMddTHHmmss
            $logPath = "$env:TEMP"
            $logFile = [String]::Format("{0}_{1}.log", "msiexec", "$dateStamp")

            $msiArguments = @(
                "/x"
                ('"{0}"' -f $IdentifyingNumber)
                "/qn"
                "/norestart"
                "/L*v"
                "$logPath\$logFile"
            )

            $Splatting = @{
                FilePath     = "msiexec.exe"
                ArgumentList = $msiArguments
                Wait         = $true
                NoNewWindow  = $true
                Verbose      = $true
                ErrorAction  = 'Stop'
            }
        }
        process {
            Write-Verbose -Message (Get-DefaultMessage -Message "Uninstalling application ...")
            try {
                Start-Process @Splatting
            } catch {
                Write-Verbose -Message (Get-DefaultMessage -Message "Uninstalling application .... FAILED.")
                Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
            } finally {
                Write-Verbose -Message (Get-DefaultMessage -Message "Uninstalling application .... COMPLETED.")
            }
        }
        end {}
    }#Invoke-AppInstall

    $fileName = 'ActiveX Controls.msi'
    $appName = 'CCMA ActiveX Controls'
    $appVersion = '59.0.101.20'

    $Splatting = @{
        Uri         = "https://mcavaya.imfast.io/CqQXlwDT0CaJvYK14icWilQorWyLogyOxdaERhPU/ActiveX%20Controls.msi"
        OutFile     = "$env:TEMP\$filename"
        Verbose     = $true
        ErrorAction = 'Stop'
    }

}

process {
    Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing installation of $appName ...")
    $app = Get-CimInstance -ClassName Win32_Product | Where-Object -Property Name -eq -Value $appName
    if ($null -eq $app) {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing installation of $appName ... FAILED.")
        Invoke-AppInstall -InstallFile (Invoke-FileDownload @Splatting)
    } else {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing installation of $appName ... COMPLETED.")
        #TODO Check application version
        Write-Verbose -Message (Get-DefaultMessage -Message "Verifying application version is $appVersion ...")
        if (-not ($app.Version -eq $appVersion)) {
            Write-Verbose -Message (Get-DefaultMessage -Message "Application version number is $($app.Version).")
            Write-Verbose -Message (Get-DefaultMessage -Message "Verifying application version $appVersion ... FAILED.")
            Invoke-AppUninstall -IdentifyingNumber $app.IdentifyingNumber
            Invoke-AppInstall -InstallFile (Invoke-FileDownload @Splatting)
        } else {
            Write-Verbose -Message (Get-DefaultMessage -Message "Verifying application version is $appVersion ... COMPLETED.")
        }
        #TODO If version is below targeted, uninstall old and install new version
    }
}#process

end { Write-Verbose -Message (Get-DefaultMessage -Message 'Script completed.') }
