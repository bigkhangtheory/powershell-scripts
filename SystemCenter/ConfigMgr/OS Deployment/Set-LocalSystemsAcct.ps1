<#
    .SYNOPSIS
        Create a local account named "SYSTEMS" for local logon and administration
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>

New-LocalUser -Name "systems" -Password '$h00k0n3$' -FullName "Systems" -Description "Local account for administration" -AccountExpires -PasswordNeverExpires

Add-LocalGroupMember -Group "Administrators" -Member "systems"
