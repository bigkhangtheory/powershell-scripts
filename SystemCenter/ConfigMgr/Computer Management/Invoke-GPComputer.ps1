<#
    .SYNOPSIS
        Refreshes Group Policy settings (Computer Configurations) on the respective device
    
    .EXAMPLE
        .\Invoke-GPComputer.ps1
    
    .NOTES
        Author:     Khang M. Nguyen
#>
Invoke-GPUpdate -Target "Computer" -RandomDelayInMinutes 0 -Force