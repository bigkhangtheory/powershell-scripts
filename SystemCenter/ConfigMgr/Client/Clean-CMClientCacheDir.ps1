<#
    .SYNOPSIS
        Clean the ConfigMgr client cache directory for items not used within the set amount of retention days

    .DESCRIPTION

    .PARAMETER RetentionDays
        Purge client cache items not refreshed within the set value

    .EXAMPLE
        # Purge items from the SCCM client cache that have not been refreshed in last 7 days
        .\Clean-CMClientCacheDir.ps1 -RetentionDays 7

    .NOTES
        Author: Khang M. Nguyen <bigkhangtheory@gmail.com>
        Last-Modified: 2020-10-23
#>

# ------------------------------------------------------------------------------
# Parameters
# ------------------------------------------------------------------------------
[CmdletBinding( SupportsShouldProcess=$True )]
param(
    [Parameter( Mandatory=$True, HelpMessage="Purge ConfigMgr client cache items not refreshed within the set value" )]
    [ValidateNotNullOrEmpty()]
    [string]$RetentionDays
)
# ------------------------------------------------------------------------------
# Begin Process
# ------------------------------------------------------------------------------
process {
    # Construct a new UIResourceMgr object and access CMClient cache
    $CMClient = New-Object -ComObject UIResource.UIResourceMgr
    if ( $CMClient -NE -$Null ) {
        if ( ($CMClient.GetType()).Name -Match "_ComObject" ) {
            # Get client cache directory location
            $CMClientCacheDir = ($CMClient.GetCacheInfo().Location)
            # Create a list of all applications due in the future or running
                $DeployedApps = $CMClient.GetAvailableApplications() | Where-Object {
                    (($_.StartTime -GT (Get-Date)) -OR ($_.IsCurrentlyRunning -EQ "1"))
                }

    }
}
