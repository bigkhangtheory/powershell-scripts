[CmdletBinding()]
Param(
  [string]$VAR,
  [string]$VAL
)

$CMModulePath = "\SCCM-MGMT.CS.ODU.EDU\SMS_CS3\AdminConsole\bin\ConfigurationManager.psd1"
Import-Module $CMModulePath

Set-Location CS3:

$Devices = Get-CMDevice -CollectionName "All Server Clients" | Where-Object {$_.Name}
New-CMDeviceVariable -DeviceName  -VariableName $VAR -VariableValue $VAL -IsMask 0
$Output = Get-CMDeviceVariable -DeviceName  -VariableName $VAR
Write-Host $Output