<#
    .SYNOPSIS
        Remote Server Administration Tools includes Server Manager, Microsoft 
        Management Console (mmc) snap-ins, consoles, Windows Powershell cmdlets
        and providers, and some command-line tools for managing roles and
        features that run on Windows Server.
    .EXAMPLE
        .\Invoke-InstallRsatTools
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>

Install-WindowsFeature -IncludeAllSubFeature RSAT