function Outer-Method {
    param (
        [System.String]$First,
        [System.String]$Second
    )
    Write-Host ($First) -NoNewline
    Inner-Method @PSBoundParameters 
}
function Inner-Method {
    param(
        [System.String]$Second
    )
    Write-Host (" {0}" -f $Second)
    Write-Host ($First)
    Write-Host ($Second)
    Write-Host ($Third)
}
$Parameters = @{
    First = "Hello"
    Second = "World"
    Third = "!"
}
Outer-Method @Parameters