<#
  .SYNOPSIS
    Installs a choclately package
  .EXAMPLE
    .\Invoke-ChocoInstall.ps1 -Package <package>
  .EXAMPLE
    .\Invoke-ChocoUpgrade.ps1 -Package "adobereader"
  .NOTES
    Khang M. Nguyen
    bigkhangtheory@gmail.com
    @bigkhangtheory (ig)
#>
[CmdletBinding()]
Param (
  [Parameter(Mandatory, ParameterSetName = 'SinglePackage')]
  [ValidateNotNullOrEmpty()]
  [string]$Package
)

Start-Process -FilePath "choco.exe" -ArgumentList "install","-y","$Package" -NoNewWindow -Wait