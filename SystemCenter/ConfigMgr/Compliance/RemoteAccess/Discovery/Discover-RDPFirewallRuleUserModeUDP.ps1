<#
    .SYNOPSIS
        Discover if firewall rule named "Remote Desktop - User Mode (UDP-In)" is enabled
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>

$FirewallRule = Get-NetFirewallRule -DisplayName "Remote Desktop - User Mode (UDP-In)" `
| Where-Object { $_.Profile -eq "Domain, Private" }

if ( $FirewallRule.Enabled -eq $True ) {
    echo "True"
}
else {
    echo "False"
}