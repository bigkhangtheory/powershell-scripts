<#
    .SYNOPSIS
        Add registry key to define a default location for the Taskbar Layout Modification file
        Copies TaskbarLayoutModification.xml file to destination location
#>
$RegistryPath = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer"
$Key = "LayoutXMLPath"
$Value = "C:\Windows\OEM\TaskbarLayoutModification.xml"

New-ItemProperty -Path Registry::HKEY_LOCAL_MACHINE\$RegistryPath -Name $Key -Value $Value -PropertyType "String" -Verbose

$SrcFile = "\\SCCM-SOURCES.CS.ODU.EDU\Resources\Scripts\Operating System Deployment\Taskbar\TaskbarLayoutModification.xml"
$DestFolder = "C:\Windows\OEM\"

If (!(Test-Path -Path $DestFolder)) {
    Write-Host "$DestFolder does not exist..." -NoNewline
    Write-Host "Creating directory...."
    New-Item -ItemType Directory -Path $DestFolder -Verbose
    Write-Host "Created directory $DeskFolder success." -ForegroundColor Green
}
Else {
    Write-Host "Directory $DestFolder exists... continuing..." -ForegroundColor Green
}

Copy-Item -Path $SrcFile -Destination $DestFolder -Verbose
