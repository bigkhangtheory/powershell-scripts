$ADTPATH = "$env:SystemRoot\System32\security\ADTServer"

Start-Process -FilePath "adtadmin.exe" -WorkingDirectory "C:\Windows\System32\security\adtserver" `
    -Verb RunAs `
    -ArgumentList "-setquery -query:`"SELECT * FROM AdtsEvent WHERE NOT ((EventId=560 AND (HeaderSid = 'S-1-5-18' OR HeaderSid = 'S-1-5-19')) OR (EventId=4565 AND (PrimarySid = 'S-1-5-18' OR PrimarySid = 'S-1-5-19')) OR (EventId=538 OR EventId=4634 OR EventId=672 OR EventId=4772 OR EventId=4768 OR EventId=680 OR EventId=4776 OR EventId=571) OR ((TargetUser LIKE '%$%') AND (EventId=624 OR EventId = 4720)) OR ((HeaderUser='System' AND ClientUser like '%$%' And TargetUser = 'TsInternetUser') AND (EventId=627 OR EventId=4723)) OR ((EventId = 538 OR EventId = 540 OR EventId=4624 OR EventId=4634) AND (String01 = ‘3’) AND HeaderUser like '%$%') OR (ClientUser LIKE '%$%' AND (EventId = 672 OR EventId = 673 OR EventId=674 OR EventId=675 OR EventId=676 OR EventId = 4768 OR EventId=4769 OR EventId=4770 OR EventId=4771 OR EventId=4772)))`"" `
    -NoNewWindow `
    -Wait

Start-Sleep -s 5

Restart-Service -Name ADTServer -Force
