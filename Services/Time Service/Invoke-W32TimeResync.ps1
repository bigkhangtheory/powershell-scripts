<#
    .SYNOPSIS
        Register NTP policies and initiate a resync operation
    .NOTES
        Khang M. Nguyen
#>

try {
    Stop-Service -Name "w32time" -Force -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}

try {
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/unregister" -Verbose -Wait -NoNewWindow -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}

try {
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/register" -Verbose -Wait -NoNewWindow -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}

try {
    Start-Service -Name "w32time" -Verbose -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}

try {
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/resync","/rediscover","/nowait" -Wait -NoNewWindow -Verbose -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}


