Option Explicit
on error resume next
Dim ErrorTrap
Dim IXVersion ' Get's the current installed IX Version
Dim AAADVersion ' Get's the current installed IX Version
Dim InstalledSoftware 'used for log
Public InstallerSteps
Dim ZipFileSize
Dim URL
Dim ForceRestartFlag
Dim oShell

If Not WScript.Arguments.Named.Exists("elevate") Then
  CreateObject("Shell.Application").ShellExecute WScript.FullName _
    , """" & WScript.ScriptFullName & """ /elevate", "", "runas", 1
  WScript.Quit
End If

DetectInstalledSoftware ' Checks the current installed software

if InstalledSoftware = "AAAD ver:59.21.259.7,IX Workplace ver:3.13.0.53" then
'	if IXVersion = "3.8.5.41" then
		'The correct version of the software is installed, no need to run the installer
	InstallerSteps ="Current software installed, Nothing TO DO"
	GenerateLog
	WScript.Quit

End If

'Check if the PC is pending a restart
Dim objSysInfo
Set objSysInfo = CreateObject("Microsoft.Update.SystemInfo")
if objSysInfo.RebootRequired = True then
	'The system is not waiting for reboot
	InstallerSteps = InstallerSteps & " No work done, System pending reboot"
	GenerateLog
	WScript.Quit
end if	

'Get PC info
Dim wshShell, strComputerName, strUserName, strUserDomain
Dim sUrl
Set wshShell = CreateObject( "WScript.Shell" )
strComputerName = wshShell.ExpandEnvironmentStrings( "%COMPUTERNAME%" )
strUserName = wshShell.ExpandEnvironmentStrings( "%USERNAME%" )
strUserDomain = wshShell.ExpandEnvironmentStrings( "%USERDOMAIN%" )

'wscript.echo " TEST"

Dim srcDirectory
srcDirectory = "C:\Scripts\temp\"
Dim NetVersion
Dim installedNetVersion
installedNetVersion = GetNetVersion
'4.7.0375

Dim blnNET
Dim MyArray, Msg

MyArray = Split(installedNetVersion, ".", -1, 1)

blnNET=0
if MyArray(0) = 4 then
	'wscript.echo "=4"
	IF MyArray(1) > 8 then
		'wscript.echo ">8"
		blnNET=1
	END If
	IF MyArray(1) = 8 then
		'wscript.echo "=8"
		'if mid(MyArray(2),1,2) > 37 then
		if mid(cint(MyArray(2)),1,3) > 304 then
			'wscript.echo ">37"
			blnNET=1
			ELSE
			'wscript.echo "<37"
			blnNET=0			
		END IF
	END If
END IF	
Err.clear


'================== Check if a user is logged in =================

Dim objWMIService1, colItems1, objItem1
Dim strUserName1, strComputer1
strComputer1 = "."
Set objWMIService1 = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer1 & "\root\cimv2")
Set colItems1 = objWMIService1.ExecQuery("Select * From Win32_ComputerSystem")
If Err Then
		'Write the error number and description to a log file and abort the script with return code 1
	Else 
	  For Each objItem1 in colItems1
	   strUserName1 = objItem1.UserName    
	  Next
End If

 if strUserName1 <> "" then
 'possible user is logged in
 strUserName = strUserName1 
 end if

'================= End check if a user is logged in ==============

If blnNET=0 then
'need to install .net
	if strComputerName & "$" = strUserName then
		InstallerSteps = InstallerSteps &  "Installing .NET 4.8 - Installing rebooting machine"
		InstallProgram "Microsoft .NET Framework 4.8 Setup" , srcDirectory & "ndp48-x86-x64-allos-enu.exe /q "
	else
		InstallerSteps = InstallerSteps &  "Installing .NET 4.8 - Installing restart required"
		InstallProgram "Microsoft .NET Framework 4.8 Setup" , srcDirectory & "ndp48-x86-x64-allos-enu.exe /q /norestart"
	end if
Else
	InstallerSteps = ".NET Detected - Installing apps"
	InstallProgram "vcredist_x86_2008.exe", srcDirectory & "vcredist_x86_2008.exe /q"
	if Err.Number <>0 then
		InstallerSteps = "Error Installing vcredist_x86_2008.exe, "
		Err.Clear
	end if 
	''InstallProgram "vcredist_x86.exe", srcDirectory & "vcredist_x86.exe /q"
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & "Error Installing vcredist_x86.exe, "
		Err.Clear
	end if 
	InstallProgram "vcredist_x86_2005_SP1.exe", srcDirectory & "vcredist_x86_2005_SP1.exe /Q "
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & "Error Installing vcredist_x86_2005_SP1.exe, "
		Err.Clear
	end if 
	InstallProgram "2005_atl80sp1-kb973923-x64.exe", srcDirectory & "2005_atl80sp1-kb973923-x64.exe /Q "
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & "Error Installing 2005_atl80sp1-kb973923-x64.exe, "
		Err.Clear
	end if 
	InstallProgram "certutil.exe", "certutil.exe -enterprise -f -addstore root " & srcDirectory & "map-smgr.cacert.crt"
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & " Error Installing certificate, "
		Err.Clear
	else
		InstallerSteps = InstallerSteps & " Cert installed, "
	end if 
	InstallProgram "AvayaWorkplaceSetup3.13.0.53.15.msi", srcDirectory & "AvayaWorkplaceSetup3.13.0.53.15.msi /norestart /qn BP=0 OP=0 SHORTCUT_DESKTOP=0 LAUNCHAPPLICATION=0"
	'BP=0 OP=0 SHORTCUT_DESKTOP=0 LAUNCHAPPLICATION=0
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & "Error Installing AvayaWorkplaceSetup3.13.0.53.15.msi, "
		'wscript.echo InstallerSteps 
	Err.Clear
	end if 
	Dim objFileSys
	Set objFileSys = CreateObject("Scripting.FileSystemObject")
	If objFileSys.FileExists("C:\programdata\Avaya\Avaya Aura Agent Desktop\AAADMSIConfig.xml") Then
		'objFileSys.DeleteFile "C:\programdata\Avaya\Avaya Aura Agent Desktop\AAADMSIConfig.xml"
		'WScript.Echo ("Old XML config found and deleted")
	Else
		'WScript.Echo ("XML Config not found")
	End If
	InstallProgram "AvayaAgentDesktopClient.msi", srcDirectory & "AvayaAgentDesktopClient.msi /norestart /qn AAADSOFTPHONE=1 MMSERVERNAME=ccmm-vip.sip.mapcommunications.com AAADUSEHTTPS=TRUE"
	if Err.Number <>0 then
		InstallerSteps = InstallerSteps & "Error Installing AvayaAgentDesktopClient.msi, "
		Err.Clear
	end if 
	'C:\Users\Public\Desktop\Avaya IX Workplace\lnk'
	DeleteAFile "C:\Users\Public\Desktop\Avaya IX Workplace.lnk"
	if Err.Number <>0 then
		'InstallerSteps = InstallerSteps & "Error removing IX Shortcuts, "
		Err.Clear
	end if 
End if


GenerateLog

wscript.quit

Sub DeleteAFile(filespec)
on error resume next
   Dim fso
   Set fso = CreateObject("Scripting.FileSystemObject")
   fso.DeleteFile(filespec)
End Sub

Sub InstallProgram (progName, progArgs)

	'Const PROC_NAME = progName
	Dim RUN_CMD
	Const SLEEP_INTERVAL_MS = 1000
	Const WAIT_TIMEOUT_MS = 1500000 ' = 1000 * 60 * 25 ms = 25 mins


	RUN_CMD = progArgs
	Dim objWshShell, objWMIService
	Dim colProcesses, objProcess
	Dim intWaited, blnProcessTerminated

	Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	Set objWshShell = WScript.CreateObject("WScript.Shell")
	objWshShell.Run RUN_CMD,0,true

	intWaited = 0
	blnProcessTerminated = False
	While intWaited < WAIT_TIMEOUT_MS And Not blnProcessTerminated
		Set colProcesses = objWMIService.ExecQuery(_
			"Select * from Win32_Process where Name='" & progName & "'")
		blnProcessTerminated = True
		For Each objProcess In colProcesses
			blnProcessTerminated = False
		Next

		WScript.Sleep(SLEEP_INTERVAL_MS)
		intWaited = intWaited + SLEEP_INTERVAL_MS
	Wend

End Sub

Function GetNetVersion

	Dim WshShell, mKey, nKey, oKey
	Dim net40clientInstall, net40clientSP
	Dim net40fullInstall, net40fullSP
	Dim net35Install, net35SP

	Dim CRLF
	CRLF= chr(10)&chr(13)
	Set WshShell = CreateObject("WScript.Shell") 
	mKey="HKLM\Software\Microsoft\"
	nKey=mKey & "NET Framework Setup\NDP\"
	oKey=mKey & "Active Setup\Installed Components\"

	On error resume next

	net40clientInstall = "Not Found"
	net40clientSP = "Not Found"
	net40clientInstall = WshShell.RegRead(nKey & "v4\client\Install")
	net40clientSP = WshShell.RegRead(nKey & "v4\client\Version")
	Session.Property("N40CLIENT") = CStr(net40clientSP)

	net40fullInstall = "Not Found"
	net40fullSP = "Not Found"
	net40fullInstall = WshShell.RegRead(nKey & "v4\full\Install")
	net40fullSP = WshShell.RegRead(nKey & "v4\full\Version")
	Session.Property("N40FULL") = CStr(net40fullSP)

	net35Install = "Not Found"
	net35SP = "Not Found"
	net35Install = WshShell.RegRead(nKey & "v3.5\Install")
	net35SP = WshShell.RegRead(nKey & "v3.5\SP")
	Session.Property("N35") = CStr(net35SP)    
	GetNetVersion="0"

	if net35Install="1" then
		GetNetVersion = net35SP
	end if
	if net40fullInstall = "1" then
		GetNetVersion=net40fullSP
	end if
	if net40clientInstall = "1" then
		GetNetVersion=net40clientSP
	end if

End Function

Sub GenerateLog
	Dim wshShell, strComputerName, strUserName, strUserDomain
	Dim sUrl
	Dim WANIP
	Set wshShell = CreateObject( "WScript.Shell" )
	strComputerName = wshShell.ExpandEnvironmentStrings( "%COMPUTERNAME%" )
	strUserName = wshShell.ExpandEnvironmentStrings( "%USERNAME%" )
	strUserDomain = wshShell.ExpandEnvironmentStrings( "%USERDOMAIN%" )
	'WScript.Echo "Computer Name: " & strComputerName

	 if strUserName1 <> "" then
		 'possible user is logged in
		 strUserName = strUserName1 
	 end if

	'URL to open....
	sUrl = "https://script.google.com/a/clementineanswers.com/macros/s/AKfycbwAaFUx1Fy_9Unf6AnXwCFetdy5qCfHx05rYT589CrYmAUxEUf_/exec?"
	
	DetectInstalledSoftware
	if InstalledSoftware ="" then
	InstalledSoftware = InstallerSteps
	end if
	'WANIP = WAN_IP



	sUrl = sUrl + "PC=" & strComputerName
	sUrl = sUrl + "&User=" & strUserName
	sUrl = sUrl + "&UserDomain=" & strUserDomain
	'sUrl = sUrl + "&runDate=" & Now
	sUrl = sUrl + "&NetVersion=" & installedNetVersion
	sUrl = sUrl + "&IPAddress=" & getPCIP
	'sUrl = sUrl + "&WAN=" & WANIP
	sUrl = sUrl + "&InstallerSteps=" & InstallerSteps
	sUrl = sUrl + "&InstalledPackages=" & InstalledSoftware 
	sUrl = sUrl + "&ScriptName=" & "avaya_installerv7.vbs"


	'POST Request to send.
	dim xHttp: Set xHttp = createobject("MSXML2.ServerXMLHTTP")
	xHttp.Open "GET", sUrl, False
	' 2 stands for SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS
	' 13056 means ignore all server side cert error
	xHttp.setOption 2, 13056
	xHttp.Send
end Sub
Function CreateFolderRecursive(FullPath)
  Dim arr, dir, path
  Dim oFs

  Set oFs = WScript.CreateObject("Scripting.FileSystemObject")
  arr = split(FullPath, "\")
  path = ""
  For Each dir In arr
    If path <> "" Then path = path & "\"
    path = path & dir
    If oFs.FolderExists(path) = False Then oFs.CreateFolder(path)
  Next
End Function

Function getPCIP
Dim strQuery, strIP
Dim objWMIService, colItems, objItem

strQuery = "SELECT * FROM Win32_NetworkAdapterConfiguration WHERE MACAddress > ''"

Set objWMIService = GetObject( "winmgmts://./root/CIMV2" )
Set colItems      = objWMIService.ExecQuery( strQuery, "WQL", 48 )

For Each objItem In colItems
    If IsArray( objItem.IPAddress ) Then
        If UBound( objItem.IPAddress ) = 0 Then
            strIP = "" & objItem.IPAddress(0)
        Else
            strIP = "" & Join( objItem.IPAddress, "," )
        End If
    End If
Next

getPCIP = strIP

End Function

Sub DetectInstalledSoftware
	Dim fso:Set fso = WScript.CreateObject("Scripting.FileSystemObject")
	InstalledSoftware=""
	If fso.FileExists("C:\Program Files (x86)\Avaya\Avaya Agent Desktop\CCAD.exe") Then
		AAADVersion = fso.GetFileVersion("C:\Program Files (x86)\Avaya\Avaya Agent Desktop\CCAD.exe")
		InstalledSoftware = InstalledSoftware & "AAAD ver:" & AAADVersion & ","
	End If
	If fso.FileExists("C:\Program Files (x86)\Avaya\Avaya IX Workplace\Avaya IX Workplace.exe") Then
		IXVersion = fso.GetFileVersion("C:\Program Files (x86)\Avaya\Avaya IX Workplace\Avaya IX Workplace.exe")
		RemoveAutoStartIX
		InstalledSoftware = InstalledSoftware & "IX Workplace ver:" & IXVersion	
	End If
End Sub

Sub RemoveAutoStartIX
	'Remove Autostart
	Dim value
	Set oShell = CreateObject("Wscript.Shell")
	If keyExists("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run\Avaya IX Workplace") Then
		Wscript.Sleep 10
		'UpdateStatus("Debug msg: IX settings Autostart Shell run is found")
		oShell.RegDelete( "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run\Avaya IX Workplace" )
		'UpdateStatus("IX settings autostart removed")
	End if
End Sub

Private Function KeyExists (keyName)
	Dim bKey
	On Error Resume Next

	bKey = oShell.RegRead(keyName)
 
	If TypeName (bKey) = "Empty" Then
		KeyExists = False
	Else
		KeyExists = True
	End If
End Function


Function WAN_IP()
    Set objxmlHTTP = CreateObject("Microsoft.XMLHTTP")
    Call objxmlHTTP.open("get", "http://checkip.dyndns.org", False)
    objxmlHTTP.Send()

    strHTMLText = objxmlHTTP.ResponseText
    Set objxmlHTTP = Nothing

    If strHTMLText <> "" Then
        varStart = InStr(1, strHTMLText, "Current IP Address:", vbTextCompare) + 19
        If varStart Then varStop = InStr(varStart, strHTMLText, "</body>", vbTextCompare)
        If varStart And varStop Then strIP = Mid(strHTMLText, varStart, varStop - varStart)
    Else
        strIP = "Unavailable"
    End If

    WAN_IP = Trim(strIP)
End Function
