# Prerequisites
<p> Ensure the you meet the requirements listed in
 [Prerequisites for Setting up the AWS Tools for Powershell](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-getting-set-up-prereq.html)

Newer versions of Powershell, including **Powershell Core** ,
 are available as downloads from Microsoft at
 [Installing various versions of PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell)
</p>
# Installation
Install a modularized version of AWS Tools for Workspaces on PowerShell.

1. Start a Powershell session.

> <p> I recommend that you *don't* run Powershell as an administator with
>  elevated permissions except when required </p>

2. To install the modularized AWS.Tools package, run the following command.
```
PS > Install-Module -Name AWS.Tools.Installer -Force

```
3. <p> You can now install the module for the AWS Workspaces service by using
 the `Install-AWSToolsModule` cmdlet. For example, the following command installs
 the **Workspaces** module. This command will also install any dependent modules
 that are required for the specified module to work. </p>
 `
 PS > Install-AWSToolsModule -Name AWS.Tools.Workspaces -Cleanup
 `

