<#
    .SYNOPSIS
        Trigger ConfigMgr client action to scan for available software updates
    .DESCRIPTION
        Invokes command to start scheduled action 113 - Software Update Scan Cycle
    .PARAMETER ComputerName
        Specifies the name of the client device
    .PARAMETER ConnectionTest
        Specifies whether to validate network connectivity to client device
    .EXAMPLE
        .\Start-SoftwareUpdateScan.ps1 -ComputerName PS1-1 -ConnectionTest $true
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>