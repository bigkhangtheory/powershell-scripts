<#
    .SYNOPSIS
        Install and configure a Chocolatey.Server package
    .DESCRIPTION
        The Chocolatey.Server package contains the binaries for a fully ready
        to go Chocolatey NuGet server where packages are served over HTTP
        using a NuGet-compatible v2 OData Atom Feed.
    .REQUIREMENTS
        * .NET Framework 4.6+
        * >= 50GB of free space on
        * >= 8GB RAM
    .PARAMETER SiteName
        Name for the website serving HTTP requests for chocolatey packages.
    .PARAMETER SitePath
        Path to store Chocolatey.Server package files.
    .PARAMETER AppName
        Name for the the IIS Appplication Pool running Chocolatey Server.
#>
[CmdletBinding()]
param
(
    [Parameter(Mandatory = $false)]
    [System.String]
    $SiteName = 'ChocolateyServer',

    [Parameter(Mandatory = $false)]
    [System.String]
    $SitePath = 'C:\Tools\Chocolatey.Server',

    [Parameter(Mandatory = $false)]
    [System.String]
    $AppName = 'ChocolateyServerAppPool'
)


begin
{
    <##region Helper Functions#>
    function Get-DefaultMessage
    {
    <#
        .SYNOPSIS
            Helper function to show default message used in VERBOSE/DEBUG/WARNING
            and prepend with a timestamp
        .PARAMETER Message
            Message to be displayed
        .INPUTS
            System.String
        .OUTPUTS

        .EXAMPLE
            Get-DefaultMessage -Message "Issue connecting to host"
    #>
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
            [System.String]$Message
        )
        begin
        {
            # Construct time for output
            $time = Get-Date -Format 'HH:mm:ss.fff'

            # Construct date for output
            $date = Get-Date -Format 'MM-dd-yyyy'

            # Construct command
            $command = (Get-Variable -Scope 1 -Name MyInvocation -ValueOnly).MyCommand.Name
        }
        process
        {
            $output = [String]::Format('{0,-12} {1,-10} {2,-28} {3,-30}', '[' + $time, $date + ']', '[' + $command + ']', '- ' + $Message)
            Write-Output $output
        }
        end {}
    }#Get-DefaultMessage

    function Add-Acl
    {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]$Path,

            [Parameter(Mandatory = $true)]
            [System.Security.AccessControl.FileSystemAccessRule]$AccessRule
        )

        Write-Verbose -Message (Get-DefaultMessage -Message "Retrieving existing ACL from $Path .....")
        try
        {
           $aclObject = Get-Acl -Path $Path -ErrorAction Stop -ErrorVariable ErrGetAcl
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Retrieving existing ACL from $Path ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        }
        finally
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Retrieving existing ACL from $Path ...... COMPLETED.")
        }

        Write-Verbose -Message (Get-DefaultMessage -Message "Adding ACL rule to $Path .....")
        try
        {
            Set-Acl -Path $Path -AclObject $aclObject.AddAccessRule($AccessRule)
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding ACL rule to $Path ...... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        }
        finally
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding ACL rule on Path ...... COMPLETED.")
        }
    }#Add-Acl


    function New-AclObject
    {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $SamAccountName,

            [Parameter(Mandatory = $true)]
            [System.Security.AccessControl.FileSystemRights]
            $Permission,

            [Parameter(Mandatory = $false)]
            [System.Security.AccessControl.AccessControlType]
            $AccessControl = 'Allow',

            [Parameter(Mandatory = $false)]
            [System.Security.AccessControl.InheritanceFlags]
            $Inheritance = 'None',

            [Parameter(Mandatory = $false)]
            [System.Security.AccessControl.PropagationFlags]
            $Propogation = 'None'
        )

        Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating ACL object for $SamAccountName .....")
        try
        {
            New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule(
                $SamAccountName, $Permission, $Inheritance, $Propogation, $AccessControl
            )
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating ACL object for $SamAccountName ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        }
        finally
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating ACL object for $SamAccountName ..... COMPLETED.")
        }
    }#New-AclObject


    Write-Verbose -Message (Get-DefaultMessage -Message "Verifying local installation of chocolatey .....")
    if ($null = (Get-Command -Name 'choco.exe'))
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Verifying local installation of chocolatey ..... FAILED.")
        Write-Verbose -Message (Get-DefaultMessage -Message "Installing chocolatey .....")
        try
        {
            Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression (
                (New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')
            )
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Installing chocolatey ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$(Error[0].Exception.Message)")
        }
        finally {
            Write-Verbose -Message (Get-DefaultMessage -Message "Installing chocolatey ..... COMPLETED.")
        }
    }#if
    else
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Verifying local installation of chocolatey ..... COMPLETED.")
    }
}#begin


process
{
    Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server prerequisites .....")
    try
    {
        Start-Process -FilePath "choco.exe" -ArgumentList "install","-y","IIS-WebServer","--source","windowsfeatures" -Verb RunAs -Wait -ErrorAction Stop -ErrorVariable ErrWebServer
        Start-Process -FilePath "choco.exe" -ArgumentList "install","-y","IIS-ASPNET45","--source","windowsfeatures" -Verb Runas -Wait -ErrorAction Stop -ErrorVariable ErrAspNet
        Start-Process -FilePath "choco.exe" -ArgumentList "install","-y","IIS-ApplicationInit","--source","windowsfeatures" -Verb Runas -Wait
    }
    catch
    {
        if ($ErrWebServer) { Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server prerequisites (IIS Web Server) ..... FAILED") }
        if ($ErrAspNet) { Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server prerequisites (IIS AspNet) ..... FAILED.") }
        Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server prerequisites ..... COMPLETED.")
    }

    Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server ..... ")
    try
    {
        Start-Process -FilePath "choco.exe" -ArgumentList "upgrade","-y","chocolatey.server" -Verb Runas -Wait -NoNewWindow -ErrorAction SilentlyContinue
    }
    catch
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server ..... FAILED.")
        Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Installing Chocolatey.Server ..... COMPLETED.")
    }

    # import the required module for IIS administration
    Import-Module -Module WebAdministration

    Write-Verbose -Message (Get-DefaultMessage -Message "Stopping the Default Web Site .....")
    try
    {
        # stop running the default web site
        Get-WebSite -Name 'Default Web Site' | Stop-WebSite
    }
    catch
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Stopping the Default Web Site ..... FAILED.")
        Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Stopping the Default Web Site ..... COMPLETED.")
        # disable the default web site from autostarting on reboots
        Set-ItemProperty -Path "IIS:\Sites\Default Web Site" -Item serverAutoStart -Value $false
    }

    Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating IIS Application Pool $AppName .....")
    try
    {
        New-WebAppPool -Name $AppName -Force
    }
    catch
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating IIS Application Pool $AppName ..... FAILED.")
        Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating IIS Application Pool $AppName ..... COMPLETED")
        <#
            Ensure 32-bit web application is enabled
            Ensure the managed runtime version is v4.0 or some version of 4
            Ensure Integrated mode is enabled
        #>
        Set-ItemProperty -Path "IIS:\AppPools\$AppName" -Item enable32BitAppOnWin64 -Value $true
        Set-ItemProperty -Path "IIS:\AppPools\$AppName" -Item managedRuntimeVersion -Value "v4.0"
        Set-ItemProperty -Path "IIS:\AppPools\$AppName" -Item managedPipelineMode -Value 'Integrated'
    }

    Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating Web Site $SiteName in $SitePath .....")
    try
    {
        # set up an IIS website pointed at the install location and
        # set it to use the application pool
        New-Website -Name $SiteName -ApplicationPool $AppName -PhysicalPath $SitePath
    }
    catch
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating Web Site $SiteName in $SitePath ..... FAILED.")
        Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Instantiating Web Site $SiteName in $SitePath ..... COMPLETED.")
    }

    # array containing IIS-related service accounts
    $iisDirAccounts = @(
        'IIS_IUSRS', 'IUSR', "IIS APPPOOL\$AppName"
    )

    # adding required permissions for IIS accounts on the web site directory
    foreach ($account in $iisDirAccounts)
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $SitePath .....")
        try
        {
            $Splatting = @{
                SamAccountName  = $account
                Permission      = 'ReadAndExecute'
                Inheritance     = 'ContainerInherit','ObjectInherit'
            }
            Add-Acl -Path $SitePath -AclObject (New-AclObject @Splatting )
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $SitePath ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$(Error[0].Exception.Message)")
        }
        finally
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $SitePath ..... COMPLETED.")
        }
    }#foreach

    # array containing IIS-related service accounts
    $iisAppAccounts = @(
        'IIS_IUSRS', "IIS APPPOOL\$AppName"
    )
    # retrive full path of IIS web site app data
    $appDataPath = Join-Path -Path $SitePath -ChildPath 'App_Data'

    # adding required permissions for IIS accounts to the App_Data subfolder
    foreach ($account in $iisAppAccounts)
    {
        Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $appDataPath .....")
        try
        {
            $Splatting = @{
                SamAccountName  = $account
                Permission      = 'Modify'
                Inheritance     = 'ContainerInherit', 'ObjectInherit'
            }
            Add-Acl -Path $appDataPath -AclObject (New-AclObject @Splatting)
        }
        catch
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $appDataPath ..... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$(Error[0].Exception.Message)")
        }
        finally
        {
            Write-Verbose -Message (Get-DefaultMessage -Message "Adding permissions for $account to $appDataPath ..... COMPLETED.")
        }
    }#foreach
}#process


end { Write-Verbose -Message (Get-DefaultMessage -Message "Script completed.")}#end

