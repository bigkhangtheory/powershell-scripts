<#
    .SYNOPSIS
        
    .DESCRIPTION
        <Long description of script>
    .PARAMETER ParamA
        <Short description of parameter input>
    .PARAMETER ParamB
        <Short description of parameter input>
    .EXAMPLE
        <Example commandline use here, repeat for more than one example>
    .INPUTS
        <Inputs if any, otherwise state None>
    .OUTPUTS
        <Outputs if any, otherwise state None>
        Example:
        Log file stored in C:\Windows\Temp\<name>.log>
    .NOTES
        Revision:       1
        Author:         <Name>
        Created:        <Creation Date>
        Updated:        <Last Modified Date>
        Changes:
            - Initial script development
    .NOTES
        All your scripts or functions should start life as something like this snippet:
            
            [CmdletBinding()]
            param()
            begin {
            }
            process {
            } 
            end {
            }
        
        You should avoid writing scripts of functions without [CmdletBinding()],
        and you should always at least consider making it take pipeline input.
    .NOTES
        Prefer: param(), begin, process, end
        Having a script written in order of execution makes the intent more clear.
        More explicite code is more maintainable over time.
#>
[CmdletBinding()]
param(
    <#
        Each parameter should be documented. To make easier to keep the comments
        synchronized with changes to parameters, the preferred location for
        parameter comments in within the 'param' block, directory above each parameter
    #>
    
    # ParamA help description
    [Parameter(Position = 0,
        ValueFromPipelineByPropertyName = $true,
        Mandatory = $true)]
    [PSObject]$ParamA,

    # ParamB help description
    [PSObject]$ParamB
)
begin {
    <#
        Use this block to initilize helper functions that can be reused within
        the script locally. Some usage examples could include functions to log
        or write out to standard output. An example is provided below:
    #>
    function Get-DefaultMessage {
        <#
        .SYNOPSIS
            Helper function to show default message used in VERBOSE/DEBUG/WARNING
            and prepend with a timestamp
        .PARAMETER Message
            Message to be displayed
        .INPUTS
            System.String
        .OUTPUTS

        .EXAMPLE
            Get-DefaultMessage -Message "Issue connecting to host"
    #>
        [CmdletBinding()]
        param(
            [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
            [System.String]$Message
        )
        begin {
            # Construct time for output
            $time = Get-Date -Format 'HH:mm:ss.fff'
            # Construct date for output
            $date = Get-Date -Format 'MM-dd-yyyy'
            # Construct context
            $context = $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)
            # Construct command
            $command = (Get-Variable -Scope 1 -Name MyInvocation -ValueOnly).MyCommand.Name
        }
        process { Write-Output "{$time} {$date} {$context} {$command} - $Message" }
        end {}
    }#Get-DefaultMessage
    
    function Get-DefaultSplatting {
        <#
        .SYNOPSIS
            Return hashtable holding default key/value pairs
        .INPUTS
            System.String
        .OUTPUTS
            System.Collections.Hashtable
        .EXAMPLE
            Get-DefaultSplatting
    #>
        [CmdletBinding()]
        param()
        return [System.Collections.Hashtable] @{
            Verbose           = $true
            InformationAction = 'Continue'
            WarningAction     = 'Continue'
            ErrorAction       = 'Stop'
            ErrorVariable     = 'ProcessError'
        }
    }#Get-DefaultSplatting
}#begin

process {
    try {
        # code goes here
    }#try
    catch {
        # catch the execption and output error with timestamp using custom function
        Write-Verbose -Message (Get-DefaultMessage -Message 'Error executing command')
        Write-Error -Message $Error[0].Exception.Message
    }#catch
    finally {
    }
}#process
end {
    Write-Verbose -Message (Get-DefaultMessage -Message 'Script completed')
}