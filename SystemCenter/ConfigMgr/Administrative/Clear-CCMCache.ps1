﻿<#
    Clear-CCMCache
    Copied from:
    https://sccm-zone.com/deleting-the-sccm-cache-the-right-way-3c1de8dc4b48

    .SYNOPSIS
    Clear the configuration manager cache

    .DESCRIPTION
    Removes all the files in the directory where
    the Configuration Manager stores its cache

    .PARAM $Path
    Specify a cache path, default is %WINDIR%\ccmcache
    Do not put a hanging '\'
    WILL REMOVE ALL FILES IN PATH GIVEN

    .EXAMPLE
    Clear-CCMCache -Path "C:\Windows\ccmcache"

#>

## Initialize the CCM resource manager com object
[__comobject]$CCMComObject = New-Object -ComObject 'UIResource.UIResourceMgr'

## Get the CacheElementIDs to delete
$CacheInfo = $CCMComObject.GetCacheInfo().GetCacheElements()

## Remove cache items
ForEach ($CacheItem in $CacheInfo) {
    $null = $CCMComObject.GetCacheInfo().DeleteCacheElement([string]$($CacheItem.CacheElementID))
}
  



