<#
    .SYNOPSIS
        Check for up-to-date version of NEWMapLogin.vbs, otherwise upgrade
    .NOTES
        Author:     Khang M. Nguyen
        Created:    2020-11-22
#>
[CmdletBinding()]
param ()

begin {
    # Helper function
    function Get-DefaultMessage {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $Message
        )
        # Construct time for output
        $time = Get-Date -Format 'HH:mm:ss.fff'
        # Construct date for output
        $date = Get-Date -Format 'MM-dd-yyyy'
        # get hostname
        $hostName = "$env:ComputerName"
        $output = [String]::Format('{0} {1} {2} {3}', '[' + $time, $date + ']', '[' + $hostName + ']', '- ' + $Message)
        Write-Output $output
    }#Get-DefaultMessage


    function Invoke-DownloadScript {
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $Uri,

            [Parameter(Mandatory = $true)]
            [System.String]
            $Destination
        )
        Write-Verbose -Message (Get-DefaultMessage -Message "Downloading script $Destination ...")
        try {
            Invoke-WebRequest -Uri $Uri -OutFile $Destination
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message "Downloading script $Destination ... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message "Downloading script $Destination ... COMPLETED.")
        }
    }#Invoke-DownloadScript

    $Splatting = @{
        Uri         = 'https://mcavaya.imfast.io/CqQXlwDT0CaJvYK14icWilQorWyLogyOxdaERhPU/NEWMapLogin3_7.vbs'
        Verbose     = $true
        ErrorAction = 'Stop'
    }

    $scriptRoot = "$env:SystemDrive\scripts"
    $scriptName = 'NEWMapLogin.vbs'
    $configSource = '\\MAPCOM.LOCAL\SYSVOL\mapcom.local\scripts\test_avaya\configdata.xml'
    $configName = 'configdata.xml'

    $shortcutPath = "$env:PUBLIC\Desktop"
    $shortcutName = 'MapLogin.vbs.lnk'
}

process {
    Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing $scriptRoot\$scriptName ...")
    # checking for existing login script file
    if (Test-Path -Path "$scriptRoot\$scriptName") {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for exist $scriptRoot\$scriptName ... COMPLETED.")

        Invoke-DownloadScript @Splatting -Destination "C:\Windows\Temp\$scriptName"

        Write-Verbose -Message (Get-DefaultMessage -Message "Verifying matching file hash ...")
        if ((Get-FileHash -Path "$scriptRoot\$scriptName").Hash -eq (Get-FileHash -Path "C:\Windows\Temp\$scriptName").Hash) {
            Write-Verbose -Message (Get-DefaultMessage -Message 'Verifying matching file hash ... COMPLETED.')
        } else {
            Write-Verbose -Message (Get-DefaultMessage -Message 'Verifying matching file hash ... FAILED.')
            Write-Verbose -Message (Get-DefaultMessage -Message "Updating $scriptRoot\$scriptName with C:\Windows\Temp\$scriptName.")
            Move-Item -Path "C:\Windows\Temp\$scriptName" -Destination "$scriptRoot\$scriptName" -Force -Verbose
        }
    } else {
        # if login does not exist, retrieve to file and store into AHA agent's script folder
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing $scriptRoot\$scriptName ... FAILED." )

        Invoke-DownloadScript @Splatting -Destination "$scriptRoot\$scriptName"
    }

    Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing Desktop shortcut $shortcutPath\$shortcutName ...")
    if (Test-Path -Path "$shortcutPath\$shortcutFile") {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing Desktop shortcut $shortcutPath\$shortcutName ... COMPLETED.")
    } else {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing Desktop shortcut $shortcutPath\$shortcutName ... FAILED.")
        Write-Verbose -Message (Get-DefaultMessage -Message "Creating Desktop shortcut $shortcutPath\$shortcutName ... ")
        try {
            $targetFile = "$scriptRoot\$scriptName"
            $shortcutFile = "$shortcutPath\$shortcutName"
            $shortcut = (New-Object -ComObject WScript.Shell).CreateShortcut($shortcutFile)
            $shortcut.TargetPath = $targetFile
            $shortcut.Save()
        } catch {
            Write-Verbose -Message (Get-DefaultMessage -Message "Creating Desktop shortcut $shortcutPath\$shortcutName ... FAILED.")
            Write-Error -Message (Get-DefaultMessage -Message "$($Error[0].Exception.Message)")
        } finally {
            Write-Verbose -Message (Get-DefaultMessage -Message "Creating Desktop shortcut $shortcutPath\$shortcutName ... COMPLETED.")
        }
    }

    Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing $scriptRoot\$configName ...")
    # checking for existing config file
    if (Test-Path -Path "$scriptRoot\$configName") {
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing $scriptRoot\$configName ... COMPLETED.")

        Copy-Item -Path $configSource -Destination "C:\Windows\Temp\$configName" -Verbose

        Write-Verbose -Message (Get-DefaultMessage -Message "Verifying matching file hash ...")
        if ((Get-FileHash -Path "$scriptRoot\$configName").Hash -eq (Get-FileHash -Path "C:\Windows\Temp\$configName").Hash) {
            Write-Verbose -Message (Get-DefaultMessage -Message 'Verifying matching file hash ... COMPLETED.')
        } else {
            Write-Verbose -Message (Get-DefaultMessage -Message 'Verifying matching file hash ... FAILED.')
            Write-Verbose -Message (Get-DefaultMessage -Message "Updating $scriptRoot\$configName with C:\Windows\Temp\$configName.")
            Move-Item -Path "C:\Windows\Temp\$configName" -Destination "$scriptRoot\$configName" -Force -Verbose
        }
    } else {
        # if login does not exist, retrieve to file and store into AHA agent's script folder
        Write-Verbose -Message (Get-DefaultMessage -Message "Checking for existing $scriptRoot\$configName ... FAILED." )

        Copy-Item -Path $configSource -Destination "$scriptRoot\$configName" -Verbose -Force
    }
}#process

end { Write-Verbose -Message (Get-DefaultMessage -Message 'Script completed.') }
