<#
  .SYNOPSIS
    Upgrades a chocolatey package or all packages. If package is not installed, then upgrade command will install it.
  .EXAMPLE
    .\Invoke-ChocoUpgrade.ps1 -Package <package>
  .EXAMPLE
    .\Invoke-ChocoUpgrade.ps1 -All
  .NOTES
    Khang M. Nguyen
    bigkhangtheory@gmail.com
    @bigkhangtheory (ig)
#>
[CmdletBinding()]
Param (
  [Parameter(Mandatory, ValueFromPipelineByPropertyName, ParameterSetName = 'AllPackages')]
  [string]$All,

  [Parameter(Mandatory, ParameterSetName = 'SinglePackage')]
  [ValidateNotNullOrEmpty()]
  [string]$Package
)

if ($All) {
  choco upgrade --confirm --nocolor --limitoutput all
}
else {
  choco upgrade --confirm --nocolor --limitoutput $Package
}