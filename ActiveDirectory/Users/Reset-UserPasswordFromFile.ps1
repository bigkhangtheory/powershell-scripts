<#
    .SYNOPSIS
        Reset password from data pulled from an Excel file
    .PARAMETER Credential
        Credentials for account granted permissions to reset user passwords.
    .PARAMETER FilePath
        Specified the path of the *.xlsx file. The file contains the user's
        full name, username, and password.
    .EXAMPLE
        Reset-UserPasswordFromFile.ps1 -FilePath "C:\user_list.xlsx"
    .NOTES
        Name:       Reset-UserPasswordFromFile.ps1
        Author:     Khang M. Nguyen
        Created:    2020-11-20
#>
[CmdletBinding(SupportsShouldProcess = $true, ConfirmImpact="High")]
param
(
    [Parameter(Mandatory = $true)]
    [Alias('RunAs')]
    [System.Management.Automation.Credential()]
    [PSCredential]
    $Credential = [System.Management.Automation.PSCredential]::Empty,

    [Parameter(Mandatory = $true)]
    [System.String]
    $FilePath
)


begin
{
    <##region Helper Functions#>
    function Get-DefaultMessage
    {
    <#
        .SYNOPSIS
            Helper function to show default message used in VERBOSE/DEBUG/WARNING
            and prepend with a timestamp
        .PARAMETER Message
            Message to be displayed
        .INPUTS
            System.String
        .OUTPUTS

        .EXAMPLE
            Get-DefaultMessage -Message "Issue connecting to host"
    #>
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
            [System.String]$Message
        )
        begin
        {
            # Construct time for output
            $time = Get-Date -Format 'HH:mm:ss.fff'

            # Construct date for output
            $date = Get-Date -Format 'MM-dd-yyyy'

            # Construct command
            $command = (Get-Variable -Scope 1 -Name MyInvocation -ValueOnly).MyCommand.Name
        }
        process
        {
            $output = [String]::Format('{0,-12} {1,-10} {2,-28} {3,-30}', '[' + $time, $date + ']', '[' + $command + ']', '- ' + $Message)
            Write-Output $output
        }
        end {}
    }#Get-DefaultMessage


    # Import Active Directory module
    Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module ActiveDirectory .....')
    try
    {
        if (-not (Get-Module -Name ActiveDirectory))
        {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module ActiveDirectory ..... FAILED.')
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module ActiveDirectory .....')
            try
            {
                Import-Module -Name ActiveDirectory -ErrorAction Stop
            }
            catch
            {
                Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module ActiveDirectory ..... FAILED.')
            }
            finally
            {
                Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module ActiveDirectory ..... COMPLETED.')
            }
        }#if
    }
    catch
    {
        Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module ActiveDirectory ..... COMPLETED.')
    }

    # Import PSExcel module
    Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module PSExcel .....')
    try
    {
        if (-not (Get-Module -Name ActiveDirectory))
        {
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module PSExcel ..... FAILED.')
            Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module PSExcel .....')
            try
            {
                Import-Module -Name ActiveDirectory -ErrorAction Stop
            }
            catch
            {
                Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module PSExcel ..... FAILED.')
            }
            finally
            {
                Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing module PSExcel ..... COMPLETED.')
            }
        }#if
    }
    catch
    {
        Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Checking for module PSExcel ..... COMPLETED.')
    }


    Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing excel file "$FilePath" .....')
    try
    {
        $excelFile = Import-XLSX -Path $FilePath -RowStart 1 -ErrorAction Stop
    }
    catch
    {
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing excel file "$FilePath" ..... FAILED.')
        Write-Error -Message (Get-DefaultMessage -Message "[BEGIN] $($Error[0].Exception.Message)")
    }
    finally
    {
        Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Importing excel file "FilePath" ..... COMPLETED.')
    }

    # instantiate a list for users
    $users = New-Object System.Collections.ArrayList

    Write-Verbose -Message (Get-DefaultMessage -Message '[BEGIN] Creating Credential hashtable.')
    $Splatting = [HashTable] @{
        Credential  = $PSBoundParameters['Credential']
        Reset       = $true
        Verbose     = $true
        ErrorAction = Stop
    }
 }#begin


process
{
    Write-Verbose -Message (Get-DefaultMessage -Message '[PROCESSING] Processing Excel file ')
    foreach ($item in $excelFile)
    {
        $users.Add($item) | Out-Null
    }

    foreach ($user in $users)
    {
        $identity = $user | Select-Object -Property "Network Login"
        $password = $user | Select-Object -Property "Network Password"
        Set-ADAccountPassword -Identity $identity -NewPassword (ConvertTo-SecureString -AsPlainText $password -Force)
    }
}
