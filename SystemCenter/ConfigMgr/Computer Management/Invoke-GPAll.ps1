<#
    .SYNOPSIS
        Refreshes Group Policy settings (All Configurations) on the respective device
    
    .EXAMPLE
        .\Invoke-GPAll.ps1
    
    .NOTES
        Author:     Khang M. Nguyen
#>
Invoke-GPUpdate -RandomDelayInMinutes 0 -Sync