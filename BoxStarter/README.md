# Boxstarter
<p>
Boxstarter leverages Chocolatey to automate the installation of software and
create **repeatable**, **scripted** Windows environments. Chocolatey makes installing 
software easy with no user intervention.
</p>

## Reboot Resiliency
<p>
The primary objective of a Boxstarter installation, is to launch an environment 
installation no matter how complex, with no interruption. To achieve this, 
Boxstarter intercepts all Chocolatey install commands and checks for pending 
reboots. If a pending reboot is detected, Boxstarter will reboot the machine 
and automatically log the user back on and resume the installation.
</p>
