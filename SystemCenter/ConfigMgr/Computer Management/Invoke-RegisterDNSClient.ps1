<#
    .SYNOPSIS
        Registers all of the IP addresses on the computer onto the a configured DNS server
    .EXAMPLE
        .\Invoke-RegisterDNSClient.ps1
    .NOTES
        Author:     Khang M. Nguyen
#>
Clear-DnsClientCache
Register-DnsClient