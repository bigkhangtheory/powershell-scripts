# Naming Convention for Active Directory Computers
DDTTSSSOXXXXXXX

_Division (D) - State (T) - Site (S) - Operating Environment (O) - Serial Number (X)_

## Division (D)
Indicates the company division the asset is associated with.

## State (T) & Site (S)
Information regarding the computer's physical location. Provides an easy way to programmatically obtain the numbers on the quantity of deployed computers per State/Site.

## Operating Environment (O)
Possible values include:
* _test_
* _development_
* _production_

## Serial Number (X)
The last 7 digits of the computer's serial number. The serial number of a Windows 10 device can be found by running the following command:
```
wmic bios get serialnumber
```

### Examples
RANYMAPP8JX9CP2
_Remote agent production desktop computer, with serial number 8JX9CP2, located in New York (site MAP)_

PRVACHEP7TR3MA1
_Payroll production desktop computer, with serial number 7TR3MA1, located in Virginia (site CHE)_

ISVASUFPXXXXXXX
_Information Systems production desktop computer, with serial number XXXXXXX, located in Virginia (site SUF)_

HDCASSSPXXXXXXX
_Help Desk production desktop computer, with serial number XXXXXXX, located in California (site SSS)_
