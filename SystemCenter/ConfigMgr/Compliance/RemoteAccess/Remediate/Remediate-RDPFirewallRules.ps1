<#
    .SYNOPSIS
        Remediate RDP firewall rules
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>

Enable-NetFirewallRule -DisplayGroup "Remote Desktop"