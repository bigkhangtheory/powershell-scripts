try {
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/query","/peers" -Wait -NoNewWindow -Verbose -ErrorAction Stop
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/query","/source" -Wait -NoNewWindow -Verbose -ErrorAction Stop
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/query","/configuration" -Wait -NoNewWindow -Verbose -ErrorAction Stop
    Start-Process -FilePath "w32tm.exe" -ArgumentList "/query","/status" -Wait -NoNewWindow -Verbose -ErrorAction Stop
}
catch {
    Write-Host "`nError Message: " $_.Exception.Message
    Write-Host "`nError in Line: " $_.InvocationInfo.Line
    Write-Host "`nError in Line Number: "$_.InvocationInfo.ScriptLineNumber
    Write-Host "`nError Item Name: "$_.Exception.ItemName
}
finally {
    $error.clear()
}