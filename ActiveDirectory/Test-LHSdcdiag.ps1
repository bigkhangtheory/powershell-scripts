﻿#Requires -Version 2.0
Function Test-LHSdcdiag
{
<#
.SYNOPSIS
    Directory Server Diagnosis using dcdiag.exe

.DESCRIPTION
    DCDiag.exe, sometimes referred to as Domain Controller Diagnostic tool, 
    can be used to check various aspects of an Active Directory domain controller.

    The script connects to each given domain controller, runs Comprehensive Tests (runs all tests), 
    including non-default tests but excluding DcPromo and RegisterInDNS (dcdiag.exe /s:$Computer /c /v /u:Username /p: Password), 
    collects the DCDiag data, and then breaking down the output of DCDiag.exe to an object Using some regex magic.
    Run it only on English or German OS Systems as the output of dcdiag.exe is language specific.

.PARAMETER ComputerName
    Directory Server to Test.

.PARAMETER Credential
    Optional, to use alternative Credentials.

.EXAMPLE
    Test-LHSdcdiag -ComputerName DC00001 -Credential (Get-Credential) | Format-Table -AutoSize

    Target         Test                   Result
    ------         ----                   ------
    DC00001        Connectivity           passed
    DC00001        Advertising            passed
    DC00001        CheckSecurityError     passed
    DC00001        CutoffServers          passed
    DC00001        FrsEvent               passed
    DC00001        DFSREvent              passed
    DC00001        SysVolCheck            failed
    DC00001        FrsSysVol              failed
    DC00001        KccEvent               passed
    DC00001        KnowsOfRoleHolders     passed
    DC00001        MachineAccount         passed
    DC00001        NCSecDesc              passed
    DC00001        NetLogons              passed
    DC00001        ObjectsReplicated      passed
    DC00001        OutboundSecureChannels passed
    DC00001        Replications           passed
    DC00001        RidManager             passed
    DC00001        Services               failed
    DC00001        SystemLog              passed
    DC00001        Topology               passed
    DC00001        VerifyReferences       passed
    DC00001        VerifyReplicas         passed
    DC00001        DNS                    passed
    ForestDnsZones CheckSDRefDom          passed
    DomainDnsZones CheckSDRefDom          passed
    Schema         CheckSDRefDom          passed
    Schema         CrossRefValidation     passed
    Configuration  CheckSDRefDom          passed
    Configuration  CrossRefValidation     passed
    contoso        CheckSDRefDom          passed
    contoso        CrossRefValidation     passed                                                       

.EXAMPLE
    Test-LHSdcdiag -ComputerName DC00001,DC00002,DClab01,DClab02 -Credential (Get-Credential) | where {$_.Test -like "sysvol*"}

    Target      Test            Result                                                                    
    ------      ----            ------                                                                    
    DC00001     SysVolCheck     passed                                                                    
    DC00002     SysVolCheck     passed                                                                    
    DClab01     SysVolCheck     passed                                                                    
    DClab02     SysVolCheck     passed       

.EXAMPLE
    To run dcdiag tests against all domain Controller of current Domain (pipeline input support).

    Get-ADDomainController -filter * | select -ExpandProperty Name | Test-LHSdcdiag -Credential (Get-Credential)
    
.INPUTS
    none to the pipeline

.OUTPUTS
    Custom PSObjects 

.NOTES

    AUTHOR  : Pasquale Lantella 
    LASTEDIT: 19.12.2017
    KEYWORDS: dcdiag
    Version : 1.0
    History :
	
.LINK
    dcdiag.exe /?

#Requires -Version 2.0
#>
   
[cmdletbinding()]  

[OutputType('PSObject')] 

Param(

    [Parameter(Position=0,Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True,
        HelpMessage='Directory Server to connect to.')]
	[alias("CN")]
	[string[]]$ComputerName = $Env:COMPUTERNAME,

    [Parameter(Position=1)]
    [System.Management.Automation.Credential()]$Credential = [System.Management.Automation.PSCredential]::Empty

   )

BEGIN {

    Set-StrictMode -Version 2.0
    ${CmdletName} = $Pscmdlet.MyInvocation.MyCommand.Name

    $OS_Language = (Get-WmiObject win32_operatingsystem).MUILanguages 
    if ($OS_Language -like 'en-*')
    {
        Write-Verbose "English OS detected."
        # Regex for English Systems
        [regex]$RegexDCDIAG = "\s*\.+\s(?<Target>\w+\.*\w+)\s(?<Result>\w+)\stest\s\r*\n*\s*(?<Test>\w+)"
    }
    elseif ($OS_Language -like 'de-*')
    {
        Write-Verbose "German OS detected."
        # Regex for German Systems
        [regex]$RegexDCDIAG = "\s*\.+\s(?<Target>\w+\.*\w+)\shat den Test\s*\r*\n*\s*(?<Test>\w+)\s*(?<Result>\w+\s*\w+)\."
    }
    else
    {
        Write-Warning "Run it only on English or German Systems as the output of dcdiag.exe is language specific."
        break
    }


} # end BEGIN

PROCESS {

    ForEach ($Computer in $ComputerName) 
    {    
        IF (Test-Connection -ComputerName $Computer -count 2 -quiet) 
        { 
            <#
                dcdiag.exe Parameters
                /a: Test all the servers in this site
                /e: Test all the servers in the entire enterprise.  Overrides /a
                /c: Comprehensive, runs all tests, including non-default tests but excluding DcPromo and RegisterInDNS. Can use with /skip
            #>
            $DCDIAG_Params = "/s:$Computer /c /v"

            if ($PSBoundParameters['Credential']) 
            {
                write-verbose "alternative Credential used."
                $networkCredential = $Credential.GetNetworkCredential()

                $UserName = $networkCredential.UserName
                $Domain = $networkCredential.Domain
                $Password = $networkCredential.Password            
          
                $DCDIAG_Params += " /u:$Domain\$UserName"
                $DCDIAG_Params += " /p:$Password"
            }
            
            $cmd = "C:\Windows\system32\dcdiag.exe" 
            Try {Get-Command -Name $cmd -ErrorAction stop | Out-Null}
            Catch {Write-Error "[$cmd] does not exist"; break}

            write-verbose "Invoke-Expression: $cmd $DCDIAG_Params"
            $DCDiag = $null 
            $DCDiag = Invoke-Expression -Command "$cmd $DCDIAG_Params"
            #$DCDiag = & "C:\Windows\system32\dcdiag.exe" $DCDIAG_Params


            $Output = @()
            $Regex_Matches = $RegexDCDIAG.Matches($DCDiag)
            foreach ($Regex_Match in $Regex_Matches)
            {
                $myobj = "" | Select-Object Target,Test,Result
                $myobj.Target = $Regex_Match.Groups.Item('Target').value
                $myobj.Test = $Regex_Match.Groups.Item('Test').value
                $myobj.Result = $Regex_Match.Groups.Item('Result').value
                $Output += $myobj 
            }
           
            # outputting object
            Write-Output $Output
        } 
        Else 
        {
            Write-Warning "\\$Computer DO NOT reply to ping" 
        } # end IF (Test-Connection -Computer $Computer -count 2 -quiet)

    } # end ForEach ($Computer in $ComputerName)	   

} # end PROCESS

END { Write-Verbose "Function ${CmdletName} finished." }

} # end Function Test-LHSdcdiag    
