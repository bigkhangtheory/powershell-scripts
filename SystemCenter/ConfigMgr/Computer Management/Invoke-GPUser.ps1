<#
    .SYNOPSIS
        Refreshes Group Policy settings (User Configurations) on the respective device
    
    .EXAMPLE
        .\Invoke-GPUser.ps1
    
    .NOTES
        Author:     Khang M. Nguyen
#>
Invoke-GPUpdate -Target "User" -RandomDelayInMinutes 0 -Force