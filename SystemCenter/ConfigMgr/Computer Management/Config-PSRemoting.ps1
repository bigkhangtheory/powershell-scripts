<#
  .SYNOPSIS
    Configure machine for remote management
  .DESCRIPTION
    Enables the WinRM service and applicable firewall rules
  .EXAMPLE
    .\Config-PSRemoting
  .NOTES
    Khang M. Nguyen
    bigkhangtheory@gmail.com
    @bigkhangtheory (ig)
#>
[CmdletBinding()]
param()

Enable-PSRemoting -Force
# Enable Firewall rules
Enable-NetFirewallRule -DisplayGroup "Remote Scheduled Tasks Management" -Direction Inbound
Enable-NetFirewallRule -DisplayGroup "Remote Service Management" -Direction Inbound
Enable-NetFirewallRule -DisplayGroup "Windows Management Instrumentation (WMI)" -Direction Inbound
