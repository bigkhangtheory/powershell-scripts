[CmdletBinding()]
Param(
  [string]$NAME,
  [string]$VAR,
  [string]$VAL
)

$CMModulePath = "\SCCM-MGMT.CS.ODU.EDU\SMS_CS3\AdminConsole\bin\ConfigurationManager.psd1"
Import-Module $CMModulePath

Set-Location CS3:

Set-CMDeviceVariable -DeviceName $env:COMPUTERNAME -VariableName $VAR -NewVariableValue $VAL
$Output = Get-CMDeviceVariable -DeviceName $env:COMPUTERNAME -VariableName $VAR
Write-Host $Output