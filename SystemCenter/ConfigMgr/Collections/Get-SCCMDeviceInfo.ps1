<#
    .SYNOPSIS
        Get all device models present in SCCM
    .DESCRIPTION
        This script will get all device models present in SCCM
    .PARAMETER SiteServer
        Site server name with the SMS provider installed
    .EXAMPLE
        .\Get-SCCMDeviceInfo.ps1 -SiteServer "SomeServer"
    .NOTES
        Name:       Get-SCCMDeviceInfo.ps1
        Author:     Khang M. Nguyen
        Date:       2019-05-20
#>
# ==============================================================================
# ARGUMENTS
# ==============================================================================
[CmdletBinding(SupportShouldProcess=$true)]
param(
  [parameter(Mandatory=$true, HelpMessage="Site server name with the SMS provider installed")]
  [ValidateNotNullOrEmpty()]
  [ValidateScript({Test-Connection -ComputerName $_ -Count 1 -Quiet})]
  [string]$SiteServer
)
# ==============================================================================
# BEGIN
# ==============================================================================
Begin {
  # determine SiteCode in WMI
  try {
    Write-Verbose "Determining SiteCode for Site Server: '$($SiteServer)'"
    # retrieve all device objects
    $SiteCodeObjects = Get-WmiObject -Namespace "root\SMS" -Class SMS_ProviderLocation -ComputerName $SiteServer -ErrorAction Stop
    foreach ($Object in $SiteCodeObjects) {
      if ($Object.ProviderForLocalSite -eq $true) {
        $SiteCode = $Object.SiteCode
        Write-Debug "SiteCode: $($SiteCode)"
      }
    }
  }
  # otherwise error exception
  catch [Exception] {
    Throw "Unable to determine SiteCode"
  }
}

