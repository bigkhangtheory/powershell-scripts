<#
  .SYNOPSIS

  .EXAMPLE

  .NOTES
#>
Import-Module RemoteDesktop

Add-RDServer -Role "RDS-RD-Server" -ConnectionBroker "RDSBROKER-1.CS.ODU.EDU"