<#
    .SYNOPSIS
        Discover the existence of cached User Profiles
    .DESCRIPTION
        Logs off all disconnected users
        Cleans registry path [HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList]
        Check for domain profiles with last login greated than 90 days
    .EXAMPLE
        .\Discover-CachedProfiles.ps1
    .NOTES
        Khang M. Nguyen
        bigkhangtheory@gmail.com
        @bigkhangtheory (ig)
#>
Clear-Host
$oldprof = $null
quser | Select-String "Disc" | ForEach { logoff ($_.tostring() -split ' +')[2] }
Clear-Content $env:windir\temp\profilecleanup.log -Force -ErrorAction SilentlyContinue
$regpath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList"
#"$(Get-Date -DisplayHint DateTime) | Performing cleanup of bad registry entries on $regpath" | Out-File $env:windir\temp\profilecleanup.log -Append
foreach ($regkey in (Get-ChildItem "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\" -Recurse)) {
    if ($regkey.Name -like "*.bak") {
        $cleanup = $regkey.Name
        #"$(Get-Date -DisplayHint DateTime) | Removing $($cleanup)" | Out-File $env:windir\temp\profilecleanup.log -Append
        #reg delete "$cleanup" /f
    }
}
"$(Get-Date -DisplayHint DateTime) | Performing old user cleanup where last login time > 90 days" | Out-File $env:windir\temp\profilecleanup.log -Append
$users = Get-WmiObject win32_userprofile
$days = "30"
foreach ($user in $users) {
    $SID = $($user.sid)
    $objSID = New-Object System.Security.Principal.SecurityIdentifier($SID)
    $objUser = $objSID.Translate([System.Security.Principal.NTAccount])
    if (($user.LastUseTime -lt $(Get-Date).Date.AddDays(-$days)) -and $user.sid -like "S-1-5-21-*") {
        $oldprof++
        #"$(Get-Date -DisplayHint DateTime) | Removing $($objUser.value)" | Out-File $env:windir\temp\profilecleanup.log -Append
        #$user.Delete()
    }
    else {
        #"$(Get-Date -DisplayHint DateTime) | Skipping $($objUser.value)" | Out-File $env:windir\temp\profilecleanup.log -Append
    }
}
if ($oldprof -ge "1") {
    return $false
}
else {
    return $true
}