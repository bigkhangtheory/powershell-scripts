<#
  .SYNOPSIS
        Generate RSoP report for all Group Policy Objects on the local machine
  .NOTES
    Khang M. Nguyen
    bigkhangtheory@gmail.com
    @bigkhangtheory (ig)
#>
Import-Module GroupPolicy

Get-GPResultantSetOfPolicy -ReportType xml -Path C:\Windows\Temp\gpresult.xml

[xml]$GPResult = Get-Content -Path C:\Windows\Temp\gpresult.xml

$GPResult.DocumentElement.ComputerResults.GPO
